/*
 * Logger Manager
 * 
 */

'use strict';

const fs = require('fs');
const net = require('net');
const http = require('http');
const util = require('util');
const zlib = require('zlib');
const crypto = require('crypto');
const stream = require('stream');
const readline = require('readline');
const EventEmitter = require('events');
const spawn = require('child_process').spawn;

function LoggerManager(options) {
    if (!(this instanceof LoggerManager))
        return new LoggerManager(options);

    if (!options) {
        options = {};
    } else if (Object.prototype.toString.call(options) !== '[object Object]') {
        console.error('Options of LoggerManager must be passed as an object');
        return;
    }

    // To ensure that a slash comes after the passed global directory string,
    // check the end of the string and add a slash if there isn't.
    this._mainDir = options.mainDir || './logs/';
    if (this._mainDir && this._mainDir.charAt(this._mainDir.length - 1) !== '/') {
        this._mainDir += '/';
    }

    this._loggers = {};
    this._listeners = {};

    EventEmitter.call(this);
}
util.inherits(LoggerManager, EventEmitter);
exports.LoggerManager = LoggerManager;

// Check the present time and process it to a fixed format
// which will be printed in log files.
function _getTime() {
    var d = new Date();
    var year = d.getFullYear();
    var month = '0' + (d.getMonth() + 1);
    var date = '0' + d.getDate();
    var hours = '0' + d.getHours();
    var minutes = '0' + d.getMinutes();
    var seconds = '0' + d.getSeconds();
    var milli = '00' + d.getMilliseconds();
    
    // The desired format of date is
    // YYYY-MM-DD hh:mm:ss.mil
    return year + '-' + 
        month.slice(month.length - 2) + '-' + 
        date.slice(date.length - 2) + ' ' + 
        hours.slice(hours.length - 2) + ':' + 
        minutes.slice(minutes.length - 2) + ':' + 
        seconds.slice(seconds.length - 2) + '.' + 
        milli.slice(milli.length - 3);
}

// Add appendices according to the options that users have configured.
function _translate(id, line, self) {
    if (line.search(self._loggers[id].contains) > -1) {
        line = self._loggers[id].tag + ' ' + line;
        
        if (self._loggers[id].timestamp) {
            line = '[' + _getTime() + '] ' + line;
        }

        return line;
    }
}

// Append a new line to the file of the path.
function _addLine(path, line) {
    fs.appendFile(path, line + '\n', function(error) {
        if (error) {
            if (error.code === 'ENOENT') {
                var arr = path.split('/');
                arr.pop();
                
                var _path = '';
                while (arr.length !== 0) {
                    var _temp = arr.shift();

                    _path = _path + _temp + '/';

                    if (_temp === '.') {
                        continue;
                    } else {
                        fs.mkdir(_path, function(error) {
                            if (error) {
                                if (error.code === 'EEXIST') { }
                                else { return; }
                            }
                        });
                    }
                }

                _addLine(path, line);
            } else {
                console.error(error);
            }
        }
    });
}

// If the path option of manager.add() isn't passed,
// creates a file with the present time as a name.
// This method generates the name.
function _createName() {
    var d = new Date();
    var year = d.getFullYear();
    var month = '0' + (d.getMonth() + 1);
    var date = '0' + d.getDate();
    var hours = '0' + d.getHours();
    var minutes = '0' + d.getMinutes();
    var seconds = '0' + d.getSeconds();

    return 'log_' + year + 
        month.slice(month.length - 2) + 
        date.slice(date.length - 2) + '_' + 
        hours.slice(hours.length - 2) + 
        minutes.slice(minutes.length - 2) + 
        seconds.slice(seconds.length - 2);
}

// Search through the path to find a file already exists.
function _checkPathSync(path) {
    var arr = path.split('/');
    var filename = arr.pop();
    var dir = arr.join('/');

    var files = fs.readdirSync(dir);
    var l = files.length;
    for (var i = 0; i < l; i++) {
        if (filename === files[i]) {
            console.error('A log file already exists in the path.');
            return false;
        }
    }
    return true;
}

LoggerManager.prototype.add = function(stream, options) {
    if (!options) {
        options = {}
    } else if (!(Object.prototype.toString.call(options) === '[object Object]')) {
        console.error('Options parameter for add function must be an object');
        return;
    }

    var self = this;
    var append = options.append === undefined ? false : options.append;
    var timestamp = options.timestamp === undefined ? true : options.timestamp;
    
    var path = options.path || _createName();
    path = self._mainDir + path;

    if (!options.append && !_checkPathSync(path)) {
        return;
    }

    var tag = options.tag || '';
    var contains = options.contains || '';
    
    var historySize = options.historySize || 100;
    if (typeof options.historySize !== 'number')
        options.historySize = 100;
    
    // Thie ID is a MD5 hash, and client must contain this information
    // to execute other functions for a logger.
    var id = crypto.createHash('md5')
                    .update(new Date().getTime().toString())
                    .digest('hex');

    self._loggers[id] = {};
    var logger = self._loggers[id];
    
    logger.path = path;
    logger.buffer = new CircularBuffer(historySize);
    logger.rl = readline.createInterface({
        input: stream
    }).on('line', function(line) {
        var str = _translate(id, line, self);

        logger.buffer.push(str);

        _addLine(logger.path, str);
    });
    logger.timestamp = timestamp;
    logger.contains = contains;
    logger.tag = tag;

    //self.emit('add');

    return id;
};

// Parse a request string by searching an user agent
// and find a browser type. According to the type of 
// browser, a suitable line break is saved to the listener.
function _parseBrowser(request, port, self) {
    var listener = self._listeners[port];
    var arr = request.split('\r\n');
    var ua;

    var l = arr.length;
    for (var i = 0; i < l; i++) {
        if (/User-Agent/.test(arr[i])) {
            ua = arr[i];
            break;
        }
    }

    if (/Trident/i.test(ua) || /msie/i.test(ua)) {
        listener._seperator = '<br>';
    } else if (/Chrome/.test(ua)) {
        listener._seperator = '\n';
    } else {
        listener._seperator = '\r\n';
    }
}

LoggerManager.prototype.run = function(id, port) {
    var self = this;

    if (!id) {
        console.error('It seems that there is no ID in passed arguments.');
        return;
    }

    var logger = self._loggers[id];
    if (!logger) {
        console.error('There is no logger with ID: ' + id + '.');
        return;
    }

    logger.server = net.createServer(function(client) {
        // If the client is connected with HTTP, an incoming message which
        // is including an user agent comes. Take it and parse it to use.
        client.on('data', function(data) {
            _parseBrowser(data.toString('utf8'), client.remotePort, self);
        });

        client.on('close', function() {
            console.log('Client from ' + client.remoteAddress + ':' + 
                        client.remotePort + ' is disconnected.');

            logger.rl.removeListener('line', self._listeners[client.remotePort]._cb);
            delete self._listeners[client.remotePort];
        });

        client.on('error', function(error) {
            console.log('An error is occurred in the client\n' + 
                        JSON.stringify(error));
        });
    });

    logger.server.listen(port, function() {
        this.on('connection', function(client) {
            console.log('A client is connected from ' + 
                        client.remoteAddress + ':' + client.remotePort);

            var listener = self._listeners[client.remotePort] = {};
            
            logger.rl.once('line', function() {
                var seperator = self._listeners[client.remotePort]._seperator;

                client.write(logger.buffer.join(seperator) + seperator);
            });

            listener._cb = function(line) {
                var str = _translate(id, line, self);

                if (str) {
                    str += self._listeners[client.remotePort]._seperator;

                    client.write(str);
                }
            };

            logger.rl.on('line', self._listeners[client.remotePort]._cb);
        });

        this.on('close', function() {
            console.log('The server is terminated');
        });

        this.on('error', function(error) {
            console.log('An error is occurred in the server ' + 
                        JSON.stringify(error));
        });
    });
};

LoggerManager.prototype.remove = function(id) {
    var logger = this._loggers[id];
    
    logger.rl.removeAllListeners();
    delete this._loggers[id];
};

LoggerManager.prototype.stop = function(id) {
    this._loggers[id].server.close();
};

LoggerManager.prototype.clear = function(id) {
    var self = this;

    var path = self._loggers[id].path;
    var arr = path.split('/');
    var filename = arr.pop();

    fs.unlink(path, function(error) {
        if (error) {
            console.error(error);
        }

        var dir;
        while (arr.length !== 0) {
            dir = arr.join('/');
            arr.pop();

            if (dir === '.' || dir === '') {
                continue;
            }
            
            // Because rmdir cannot access to restricted paths
            // and remove non-empty directories, it is automatically
            // removes only empty directories.
            fs.rmdir(dir, function(error) {
                if (error) {
                    if (error.code === 'ENOENT');
                    else {
                        console.error(error);
                        return;
                    }
                }
            });
        }
    });
};

LoggerManager.prototype.download = function(options) {
    if (!options.id) {
        return;
    }

    var self = this;
    var path = self._loggers[options.id].path;
    var filename = path.split('/').pop() + '.gz';
    var filter = options.filter || '';

    var gzip = zlib.createGzip();
    var transform = new FilterStream({ filter: filter });

    var server = http.createServer(function(request, response) {
        response.on('close', function() {
            console.log('response end');
        });
        
        // To close the download server because remained download servers could
        // make the performance of SDK server down.
        response.on('finish', function() {
            response.end();
            server.close();
            request.socket.destroy();
        });
        
        response.setHeader('Content-disposition', 'attachment; filename=' + filename);
        
        fs.createReadStream(path)
            .pipe(transform)
            .pipe(gzip)
            .pipe(response);
    }).listen(8000);
};

// When the client is checking for logs in real time, there're probably
// missing logs because connecting to the logging server might take some time.
// Therefore, all logs are saved to a kind of buffer called ring buffer and
// print it for the first when a client is newly connected.
function CircularBuffer(size) {
    if (Object.prototype.toString.call(size) !== '[object Number]') {
        console.error('The size of CircularBuffer must be a number');
        return;
    }

    this._size = size;
    this._buffer = [];
}

CircularBuffer.prototype.push = function(data) {
    if (this._buffer.length > this._size) {
        this._buffer.shift();
    }

    this._buffer.push(data);
};

CircularBuffer.prototype.join = function(seperator) {
    return this._buffer.join(seperator);
};

// Download will be progressed by piping through the response
// so that a stream which converts and filters the data is needed. 
function FilterStream(options) {
    this._options = options;

    stream.Transform.call(this);
}
util.inherits(FilterStream, stream.Transform);

FilterStream.prototype._transform = function(chunk, encoding, done) {
    var self = this;

    encoding = 'utf8';
    
    // chunk comes from the pipe is the buffer of multiple lines,
    // so convert it to a string encoded UTF-8.
    chunk = chunk.toString(encoding);
    var lines = chunk.split(/\r\n|\r|\n/g);
    
    while (lines.length !== 0) {
        var str = lines.shift();

        if (str.search(self._options.filter) > -1) {
            self.push(str + '\r\n');
        }
    }

    done();
};
