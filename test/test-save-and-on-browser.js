/*
 * After running this test for a while, check the path './logs/foo/'.
 * All logs will be saved to the 'bar.log' in the above directory.
 *
 * Also, connecting to the server with any browsers shows the logs in real time.
 * ex) http://(address):8101
 */

'use strict';

const fork = require('child_process').fork;
const lm = require('./logger_manager.js');

(function() {
    var manager = lm.LoggerManager({
        mainDir: './logs/'
    });;
    
    var child = fork('./log_generator.js'), { silent: true });
    var id = manager.add(child.stdout, {
        tag: 'test',
        append: true,
        path: 'foo/bar.log'
    });
    
    manager.run(id, 8101);
})();