/*
 * After 10 seconds, a download server is opened with the stdout message 'Download server is opened'.
 * Then, connect to 'http://(address):8000' to download a saved log file.
 * If '//' of line 29 is removed, the downloaded log file only contains lines with 'Java' or 'JavaScript'.
 */

'use strict';

const fork = require('child_process').fork;
const lm = require('./logger_manager.js');

(function() {
    var manager = lm.LoggerManager({
        mainDir: './logs/'
    });
    
    var child = fork('./log_generator.js'), { silent: true });
    var id = manager.add(child.stdout, {
        tag: 'test',
        append: true,
        path: 'foo/bar.log'
    });
    
    setTimeout(function() {
		console.log('Download server is opened');
		
		manager.download({
			id: id
			//,filter: 'Java'
		});
	}, 10000);
})();