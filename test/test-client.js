/*
 * This is a simple example of connecting and getting real-time logs.
 * The result is printed logs in the console.
 */

const net = require('net');

net.connect({
    port: // Enter the opened port here,
    host: // Enter the host name of the server here
}, function() {
    this.on('data', function(data) {
        process.stdout.write(data.toString('utf8'));
    });
});