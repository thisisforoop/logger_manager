/*
 * This test is extension of 'test-save-and-on-browser.js' with a two lines added.
 * However in this test, you cannot access from browser because the TCP server is closed
 * due to manager.stop() and logs keep being saved to the path.
 */
'use strict';

const fork = require('child_process').fork;
const lm = require('./logger_manager.js');

(function() {
    var manager = lm.LoggerManager({
        mainDir: './logs/'
    });;
    
    var child = fork('./log_generator.js'), { silent: true });
    var id = manager.add(child.stdout, {
        tag: 'test',
        append: true,
        path: 'foo/bar.log'
    });
    
    manager.run(id, 8101);
    manager.stop(id);
})();