function generateLog() {
    setTimeout(function() {
        var log = generateRandomLog();
        console.log(log.date + ' ' + log.tag + ' ' + log.content);
        generateLog();
    }, Math.floor(Math.random() * 1000);
}

function generateRandomLog() {
    var temp = Math.floor(Math.random() * 6);
    var obj = {
        date: new Date().getTime();
    };
    
    if (temp === 0) {
        obj.tag = 'JavaScript';
        obj.content = 'by Brendan Eich';
    } else if (temp === 1) {
        obj.tag = 'C';
        obj.content = 'by Dennis Ritchie';
    } else if (temp === 2) {
        obj.tag = 'Java';
        obj.content = 'by James Gosling';
    } else if (temp === 3) {
        obj.tag = 'Python';
        obj.content = 'by Guido van Rossum';
    } else if (temp === 4) {
        obj.tag = 'Perl';
        obj.content = 'by Larry Wall';
    } else {
        obj.tag = 'Enquire';
        obj.content = 'by Tim Berners-Lee';
    }
}

generateLog();