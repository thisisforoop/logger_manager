/*
 * This test shows the difference between manager.remove() and manager.clear().
 * Two children are forked and the one is treated with manager.remove() and the other is
 * treated with manager.clear().
 * After 5 seconds, this process will be killed automatically.
 * Then, check the directory './logs/foo/' and you'll see the remain is the one which
 * has been treated with manager.remove().
 * manager.clear() clears log by deleting, but manager.remove() removes a logger from
 * the logger manager so that logs won't be saved.
 */
 
'use strict';

const fork = require('child_process').fork;
const lm = require('./logger_manager.js');

(function() {
    var manager = lm.LoggerManager({
        mainDir: './logs/'
    });;
    
    var child = fork('./log_generator.js', { silent: true });
    var id = manager.add(child.stdout, {
        tag: 'test',
        append: true,
        path: 'foo/bar.log'
    });
    
    child = fork('./log_generator.js', { silent: true });
    var id2 = manager.add(child.stdout, {
        tag: 'test',
        append: true,
        path: 'foo/bar2.log'
    })
    
    process.on('SIGINT', function() {
        manager.clear(id);
        manager.remove(id2);
        process.exit(0);
    });
    
    setTimeout(function() {
        process.kill(process.pid, 'SIGINT');
    }, 5000);
})();