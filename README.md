###※ Warning: This module isn't tested enough with a real environment so that it is not flexible and not robust.###

#Logger Manager#

To use this module, download to your project path and do
```
#!javascript
require('./logger_manager.js');
```

For example, below code is a basic usage of this module.
In this example, logs of the forked child process are saved to ./logs/foo/bar.log
and a TCP server is opened, where clients can check logs in real time.

```
#!JavaScript
const fork = require('child_process').fork;
const lm = require('logger_manager');

var manager = new lm.LoggerManager({
	mainDir: './logs/'
});

/* 
 * When a child process is forked with fork() method of child_process module, 
 * silent option should be true to rightly pipe into the parent process.
 */
var child = fork('source_file_to_build', { silent: true });
manager.add(child.stdout, {
	path: foo/bar.log
});

manager.run(child, 8101);
```

###API Docs###
* Logger Manager
	
	 Logger Manager inherits EventEmitter so that this can fire and handling events. However, it seems that there isn't a suitable place to fire events,
	so it is left as firing no event.
	
	* Class: logger_manager.LoggerManager
		* new logger_manager.LoggerManager(options)
			- (Object) options: an object for configurations of the logger added.
			options have following defaults:
			----------------------------------------------
			{  
			  	mainDir: './logs/'  
			}  
			----------------------------------------------
			
			>[optional] (string) mainDir: The directory that all logs will be managed inside.
			
			Creates a new Logger Manager managing logs in the directory of options.mainDir.  

		* manager.add(stream[, options])
			- (stream.Writable) stream: a standard output stream object from child process which can be accessed with process.stdout.
			- (Object) options: an object for configurations of the logger added. 
			 options have following defaults: 
			----------------------------------------------
			{  
				append: false,  
				timestamp: true,  
				path: 'log_YYYYMMDD_hhmmmss',  
				tag: '',  
				contains: '',  
				historySize: 100  
			}  
			----------------------------------------------
			
			>[optional] (boolean) append: Enables appending mode. When this option is off(false), a logger cannot be added to the manager 
										if a file already exists in the path. When this option is on, logs are appended to the existing file
										so that a logger can be added. 
										
			>[optional] (boolean) timestamp: Determines whether the current time is expressed in a log file or not. 
			
			>[optional] (string) path: Full path of a saved log file. Therefore, it should contain both a directory name and a file name. Also,
									the directory is created under the global directory which is configured when the Logger Manager object is
									created so that the path should not be opened with the slash('/').
									
			>[optional] (string) tag: Additional content for the log. A tag is added to the next of timestamp.  
			
			>[optional] (string) contains: Filters logs with this string. Only logs which contain the string of this option can be taken by the logger.
			
			>[optional] (number) historySize: Determines the maximal number of stacked old logs which is printed before a new log is printed. Therefore, 
											the client can check missing logs. 
											 The stacked old logs are managed by a circular buffer (ring buffer), so if the number of logs is maxed out,
											the oldest one will be deleted from the buffer.
			 
			 Add a child process to Logger Manager and start to save logs to a file in a designated path.
			 
			 At the end of the function, an ID is returned and a client should manage this ID to access the added logger.
			The ID is the MD5 hash with the content of timestamp, which is encoded with hex encoding.
				

			 After adding is completed, logs from the child process immediately starts to be saved
			to the configured path. However, this method doesn't mean that it opens a TCP server where clients can check logs in real time.
			This only ensures that it creates a log file when options are valid.
		  
		* manager.clear(id)
			 
			 - (string) id: id argument is a MD5 hash string which has been returned by manager.add() method.
			 
			 Deletes the log file and empty directories with the information gotten from the received id. 

			 It is accomplished by reaching up using repetition from the bottom using fs.unlink and fs.rmdir. Because fs.rmdir cannot remove the directory, 
			non-empty directories will be remained with log files of other loggers.
			   
		* manager.download(options)  
			 ( ※ Warning - Experimental: Unsure that a HTTP server is non-blocking and the timeout is two minutes(default). 
										Also, a port allocation problem for multiple download requests is unsolved. )
			 
			- (object) options: options argument is an object for download setup.
			 options have following defaults:
			
			----------------------------------------------
			{  
				id: undefined,  
				filter: ''  
			}  
			----------------------------------------------
			
			>[required] (string) id: A MD5 hash string which has been returned by manager.add() method. Log files can be reached by an object property whose name
									is an ID, so this string must be filled.
									
			>[optional] (string) filter: A string which is used to search a line from the log file. It is similar to 'contains' property of options for manager.add().
										Only lines including this string will be written to the downloaded file.
			 
			Opens a download HTTP server and allows to connect to the server so that a client can download the desired log file.
			 After the download server has been created by this method, the server receives a download request for only once.
			  
		* manager.remove(id)
			- (string) id: id argument is a MD5 hash string which has been returned by manager.add() method.
			
			 Removes a logger with the passed id.
			 
			 This method deletes a logger with passed id by removing all listeners of the logger and finally deleting the logger. After this is called, appending to
			the log file is ended.
			   
		* manager.run(id, port)
			- (string) id: id argument is a MD5 hash string which has been returned by manager.add() method.
			- (number) port: port argument is a port number of host.
							It should be automatically allocated, but the problem is unsolved like manager.download().
			
			 Opens a TCP server with contents of the logger with the passed id. A TCP server is opened with a address of server and passed port.
			
			 It is basically a TCP server so that it cannot flexibly react to various connections. However, as implemented for a HTTP user agent, information might be parsed
			if a data has a fixed format. In here, an user agnet data is parse to flexibly react to various browsers because browsers have different environments to show texts.
			For example, Chrome shows texts in <pre> tag so that a simple line break like '\n' works well, but IE shows texts in plain <body> tag so that a line break must be
			<br>.
			
			 After this method is called, the client can be connected into the server to check logs. Also, if another client knows the address and port, it is allowed to
			access to the TCP server to check logs.
			  
		* manager.stop(id)  
			( ※ Warning - Experimental )
			- (string) id: id argument is a MD5 hash string which has been returned by manager.add() method.
			
			 Closes the TCP server which has been opened by manager.run() method. 
			
			 It is designed to close unnecessary server. Therefore, this must be called after the logger with this id string starts running by manager.run() method.
			However, it doesn't mean that the logger related to the closed server is removed.